import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './header/navbar/navbar.component';
import { AdminComponent } from './admin/admin.component';
import { SearchService } from './services/search.service';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthService } from './auth/auth.service';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { AirlinesComponent } from './booking/airlines/airlines.component';
import { ConfirmComponent } from './booking/confirm/confirm.component';
import { DatesComponent } from './booking/dates/dates.component';
import { PaymentComponent } from './booking/payment/payment.component';
import { BookingComponent } from './booking/booking.component';
import { AirportsComponent } from './flights/airports/airports.component';
import { CreateComponent } from './flights/airports/create/create.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    AdminComponent,
    LoginComponent,
    RegisterComponent,
    AirlinesComponent,
    ConfirmComponent,
    DatesComponent,
    PaymentComponent,
    BookingComponent,
    AirportsComponent,
    CreateComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [SearchService,AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
